package pt.ulusofona.heilab.xperis.plugin;

import pt.ulusofona.heilab.xperis.service.dto.ExperimentDataDTO;
import pt.ulusofona.heilab.xperis.service.dto.ExperimentMetaDataDTO;
import pt.ulusofona.heilab.xperis.service.spi.PluginViewer;

import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class CSVViewer implements PluginViewer {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(ZoneId.systemDefault());

    public CSVViewer() {
    }

    @Override
    public byte[] view(ExperimentMetaDataDTO experimentMetaData) {
        StringBuilder result = new StringBuilder();
        result.append("Date;Participant;");
        result.append(experimentMetaData.getDataStructure());
        result.append("\r\n");
        for (ExperimentDataDTO data : experimentMetaData.getExperimentData()) {
            if (data.getData()!=null) {
                result.append(formatter.format(data.getCreatedDate())).append(";");
                result.append(data.getParticipants()).append(";");
                result.append(new String(data.getData()));
                result.append("\r\n");
            }
        }
        return result.toString().getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String getMimeTypeIn() {
        return "text/csv";
    }

    @Override
    public String getMimeTypeOut() {
        return "text/csv";
    }
}
